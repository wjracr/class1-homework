import java.util.Scanner;

public class Yang_Hui_triangle {
    public static void main(String[] args) {
        Scanner scanner=new Scanner(System.in);
        System.out.println("Please enter Line num:");
        int num=scanner.nextInt();
        int total_output=1;
        for (int space_num=(num-1);space_num>=0;space_num--){
            for (int output_space=1;output_space<=space_num;output_space++){
                System.out.print(" ");
            }
            for (int output=1;output<=2*total_output-1;output++) {
                System.out.print("*");
            }
            total_output++;
            System.out.println();
        }
    }
}
