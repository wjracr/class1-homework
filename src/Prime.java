public class Prime {
    public static void main(String[] args) {
        boolean is_prime=true;
        for (int num=101;num<=200;num++){
            for (int divisor=2;divisor<num;divisor++){
                if (num%divisor==0){
                    is_prime=false;
                    break;
                }
            }
            if (is_prime){
                System.out.println(num);
            }
            is_prime=true;
        }
    }
}
